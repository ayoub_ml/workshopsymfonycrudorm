<?php

namespace EspritParcBundle\Controller;

use EspritParcBundle\Entity\Modele;
use EspritParcBundle\Form\ModeleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ModeleController extends Controller
{
    public function readAction()
    {
        $modeles=$this->getDoctrine()->getRepository(Modele::class)->findAll();
        return $this->render('@EspritParc/Modele/read.html.twig', array(
            'modeles'=>$modeles
        ));
    }


    public function createAction(Request $request)
    {
        $modele=new Modele();
        $form=$this->createForm(ModeleType::class,$modele);
        $form=$form->handleRequest($request);
        if($form->isValid()){
            $em=$this->getDoctrine()->getManager();
            $em->persist($modele);
            $em->flush();
            return $this->redirectToRoute('read');
        }
        return $this->render('@EspritParc/Modele/create.html.twig',
            array(
                'form'=>$form->createView()
            ));
    }

    public function updateAction(Request $request , $id)
    {
        $em=$this->getDoctrine()->getManager();
        $modele = $em->getRepository(Modele::class)->find($id);
        if ($request->isMethod('POST')) {
            $modele->setPays($request->get('pays'));
            $modele->setLibelle($request->get('libelle'));
            $em->flush();
            return $this->redirectToRoute('read');
        }
        return $this->render('@EspritParc/Modele/update.html.twig', array(
            'modele' => $modele
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $modele= $em->getRepository(Modele::class)->find($id);
        $em->remove($modele);
        $em->flush();
        return $this->redirectToRoute("read");
    }

    function queryBuilderAction(){
        $em=$this->getDoctrine()->getManager();
        $modeles= $em->getRepository(Modele::class)->findPaysQB();
        return $this->render('@EspritParc/Modele/AfficheQB.html.twig',array('modeles'=>$modeles));
    }

}
